#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include<QJsonObject>
#include<QUdpSocket>
#include<QJsonDocument>
#include<QJsonArray>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);
    void server1();
    QJsonDocument doc();


signals:

public slots:
    void recieveAvg();
private:
    QUdpSocket *socket;
    int number;


};

#endif // SERVER_H
